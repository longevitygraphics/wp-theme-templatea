<div class="main-navigation">
	<nav class="navbar navbar-expand-xl navbar-dark">
		<a class="navbar-brand mr-auto mr-lg-0" href="/"><?php echo site_logo(); ?></a>

		<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button" data-toggle="offcanvas">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</button>

		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse" id="main-navbar" >
			<?php

			$mainMenu = array(
				// 'menu'              => 'menu-1',
				'theme_location' => 'top-nav',
				'depth'          => 2,
				'container'      => false,
				/*'container' => 'div',
				'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
				'container_id' => 'main-navbar',*/
				'menu_class'     => 'navbar-nav',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			);
			wp_nav_menu( $mainMenu );

			?>
		</div>

		<div class="wp-block-button is-style-fill px-2"><a class="wp-block-button__link has-text-color has-white-color has-background has-secondary-background-color px-4">Book Now</a></div>
	</nav>
</div>