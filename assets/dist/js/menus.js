/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/src/js/menus.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/src/js/menus.js":
/*!********************************!*\
  !*** ./assets/src/js/menus.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Ready Handler
(function ($) {
  $(document).ready(function () {
    // Uploading files
    var file_frame;
    var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id

    var set_to_post_id = $("#lg-service-archive-banner").attr("data-id");
    jQuery("#upload_image_button").on("click", function (event) {
      event.preventDefault(); // If the media frame already exists, reopen it.

      if (file_frame) {
        // Set the post ID to what we want
        file_frame.uploader.uploader.param("post_id", set_to_post_id); // Open frame

        file_frame.open();
        return;
      } else {
        // Set the wp.media post id so the uploader grabs the ID we want when initialised
        wp.media.model.settings.post.id = set_to_post_id;
      } // Create the media frame.


      file_frame = wp.media.frames.file_frame = wp.media({
        title: "Select a image to upload",
        button: {
          text: "Use this image"
        },
        multiple: false // Set to true to allow multiple files to be selected

      }); // When an image is selected, run a callback.

      file_frame.on("select", function () {
        // We set multiple to false so only get one image from the uploader
        attachment = file_frame.state().get("selection").first().toJSON(); // Do something with attachment.id and/or attachment.url here

        $("#image-preview").attr("src", attachment.url).css("width", "auto");
        $("#image_attachment_id").val(attachment.id);
        $("#lg-service-archive-banner").val(attachment.id); // Restore the main post ID

        wp.media.model.settings.post.id = wp_media_post_id;
      }); // Finally, open the modal

      file_frame.open();
    }); // Restore the main ID when the add media button is pressed

    jQuery("a.add_media").on("click", function () {
      wp.media.model.settings.post.id = wp_media_post_id;
    });
  });
})(jQuery);

/***/ })

/******/ });
//# sourceMappingURL=menus.js.map