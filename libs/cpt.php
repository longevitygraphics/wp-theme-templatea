<?php
/**
 * Functions file that defines CPT for theme
 *
 * @package lg_template_a
 */

?>
<?php
/**
 * Init Custom Post Types for Theme
 */
function lg_template_init_cpts() {

	$lg_testimonial_labels = array(
		'name'                  => _x( 'Testimonial', 'Post type general name', 'lg-templatea' ),
		'singular_name'         => _x( 'Testimonial', 'Post type singular name', 'lg-templatea' ),
		'menu_name'             => _x( 'Testimonials', 'Admin Menu text', 'lg-templatea' ),
		'name_admin_bar'        => _x( 'Testimonials', 'Add New on Toolbar', 'lg-templatea' ),
		'add_new'               => __( 'Add New', 'lg-templatea' ),
		'add_new_item'          => __( 'Add New Testimonial', 'lg-templatea' ),
		'new_item'              => __( 'New Testimonial', 'lg-templatea' ),
		'edit_item'             => __( 'Edit Testimonial', 'lg-templatea' ),
		'view_item'             => __( 'View Testimonial', 'lg-templatea' ),
		'all_items'             => __( 'Testimonials', 'lg-templatea' ),
		'search_items'          => __( 'Search Testimonial', 'lg-templatea' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'lg-templatea' ),
		'not_found'             => __( 'No testimonials found.', 'lg-templatea' ),
		'not_found_in_trash'    => __( 'No testimonials found in Trash.', 'lg-templatea' ),
		'featured_image'        => _x( 'Testimonial Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'archives'              => _x( 'Testimonial archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'lg-templatea' ),
		'insert_into_item'      => _x( 'Insert into Testimonial', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'lg-templatea' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Testimonial', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'lg-templatea' ),
		'filter_items_list'     => _x( 'Filter Testimonial list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'lg-templatea' ),
		'items_list_navigation' => _x( 'Testimonial list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'lg-templatea' ),
		'items_list'            => _x( 'Testimonial list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'lg-templatea' ),
	);

	$lg_services_labels = array(
		'name'                  => _x( 'Service', 'Post type general name', 'lg-templatea' ),
		'singular_name'         => _x( 'Service', 'Post type singular name', 'lg-templatea' ),
		'menu_name'             => _x( 'Services', 'Admin Menu text', 'lg-templatea' ),
		'name_admin_bar'        => _x( 'Services', 'Add New on Toolbar', 'lg-templatea' ),
		'add_new'               => __( 'Add New', 'lg-templatea' ),
		'add_new_item'          => __( 'Add New Service', 'lg-templatea' ),
		'new_item'              => __( 'New Service', 'lg-templatea' ),
		'edit_item'             => __( 'Edit Service', 'lg-templatea' ),
		'view_item'             => __( 'View Service', 'lg-templatea' ),
		'all_items'             => __( 'Services', 'lg-templatea' ),
		'search_items'          => __( 'Search Services', 'lg-templatea' ),
		'parent_item_colon'     => __( 'Parent Service:', 'lg-templatea' ),
		'not_found'             => __( 'No services found.', 'lg-templatea' ),
		'not_found_in_trash'    => __( 'No services found in Trash.', 'lg-templatea' ),
		'featured_image'        => _x( 'Service Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'lg-templatea' ),
		'archives'              => _x( 'Service archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'lg-templatea' ),
		'insert_into_item'      => _x( 'Insert into Service', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'lg-templatea' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Service', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'lg-templatea' ),
		'filter_items_list'     => _x( 'Filter Service list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'lg-templatea' ),
		'items_list_navigation' => _x( 'Service list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'lg-templatea' ),
		'items_list'            => _x( 'Service list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'lg-templatea' ),
	);

	$lg_testimonial_args = array(
		'labels'             => $lg_testimonial_labels,
		'description'        => 'Testimonial Custom Post Type.',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonial' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'author' ),
		'delete_with_user'   => false,
		'taxonomies'         => array( 'category' ),
		'show_in_rest'       => true,
		'menu_icon'          => 'dashicons-testimonial',
	);

	$lg_services_args = array(
		'labels'             => $lg_services_labels,
		'description'        => 'Service Custom Post Type.',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'service' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'         => array( 'lg-service-category' ),
		'show_in_rest'       => true,
		'menu_icon'          => 'dashicons-hammer',
	);

	register_post_type( 'lg-testimonial', $lg_testimonial_args );

	register_post_type( 'lg-service', $lg_services_args );

	register_taxonomy(
		'lg-service-category',
		'lg-service',
		array(
			'label'              => __( 'Service Category' ),
			'rewrite'            => array(
				'slug'       => 'lg-service-category',
				'with_front' => false,
			),
			'hierarchical'       => true,
			'show_in_rest'       => true,
			'show_ui'            => true,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,

		)
	);

}

add_action( 'init', 'lg_template_init_cpts' );




